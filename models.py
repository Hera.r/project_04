from datetime import datetime
from json import dumps
from tinydb import TinyDB

chess_db = TinyDB('db.json')
table_tournois = chess_db.table('tournois')
table_players = chess_db.table('players')
table_rounds = chess_db.table('rounds')
table_match = chess_db.table('match')


class Tournament:
    def __init__(self, name, location,
                 time_control, description, number_of_turns=4):
        self.name = name
        self.location = location
        self.date = datetime.now()
        self.number_of_turns = number_of_turns
        self.list_of_rounds = []  # liste de d'instance de round
        self.players = {}  # dico de joueur avec et ses points
        self.time_control = time_control  # choix bullet et blitz ou speed
        self.description = description
        self.serialized_tournaments = {
            "name": self.name,
            "location": self.location,
            "date": dumps(self.date, default=str),
            "number_of_turns": self.number_of_turns,
            "list_of_rounds": self.list_of_rounds,
            "players": self.players,
            "time_control": self.time_control,
            "descritpion": self.description

        }

    def __str__(self):
        return "{}".format(self.date)


class Players:
    def __init__(self, last_name, first_name, birth_date, gender, ranking=0):
        self.last_name = last_name
        self.first_name = first_name
        self.birth_date = birth_date
        self.gender = gender
        self.ranking = ranking
        self.serialized_players = {
            "last_name": self.last_name,
            "first_name": self.first_name,
            "birth_date": self.birth_date,
            "gender": self.gender,
            "ranking": self.ranking
        }

    def __str__(self):
        return "{}, {}, {}, {}".format(self.last_name,
                                       self.first_name,
                                       self.birth_date, self.gender)


class Round:
    def __init__(self, name):
        self.name = name
        self.date = datetime.now()
        self.time_bigin = datetime.now()  # heure debut
        self.end_time = datetime.now()  # heure de fin
        self.list_match = []
        self.serialized_rounds = {
            "name": self.name,
            "date": dumps(self.date, default=str),
            "time_bigin": dumps(self.time_bigin, default=str),
            "end_time": dumps(self.end_time, default=str),
            "list_match": self.list_match
        }


class Match:
    def __init__(self, name, player1, player2, score_player1=0,
                 score_player2=0):
        self.name = name
        self.player1 = player1
        self.player2 = player2
        self.vs_players = {
            player1.last_name: score_player1, player2.last_name: score_player2}

        self.serialized_match = {
            "name": self.name,
            "vs_Players": self.vs_players
        }
