
class AView:
    def welcome(self):
        print('//////////////////////////////////////////////////////////')
        print(" "*10, "Bienvenue sur le Gestionnaire de Tournois")
        print("//////////////////////////////////////////////////////////\n")

    def display_vs(self, current_round):

        for i in range(0, len(current_round[0]["list_match"]), 2):
            print(current_round[0]["list_match"][i], " VS ",
                  current_round[0]["list_match"][i + 1])
        print("\n")

    def end_match(self):
        print('**********************************************************')
        print("Fin ce Match")
        print("Entrer le score des joueurs")
        print('**********************************************************\n')

    def menu(self):
        print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
        print("Menu Du Match")
        print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n')

    def round_end(self, round_name):
        print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
        print("Fin du round " + round_name)
        print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n')

    def display_players(self, all_players):
        print("+++++++ la liste des joueurs ++++++++++")
        all_p = []
        for player in all_players:
            all_p.append(player["last_name"])

        all_p.sort()
        print("\n".join(all_p))
        print('\n')

    def display_ranking(self, rank_players):
        if len(rank_players) == 0:
            print("Le Classement des joueurs n'est pas encore disponible")
        else:
            rank = dict(
                sorted(rank_players.items(), key=lambda item: item[1],
                       reverse=True))
            i = 1
            for k, v in rank.items():
                print(str(i), ' => ', k, "nombre de points: ", v)
                i += 1
