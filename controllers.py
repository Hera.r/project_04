from models import Tournament, Players, Round, Match
from models import table_tournois, table_players, table_rounds, table_match
from tinydb import Query
from tinydb.operations import add


class Controller:
    def menu_match(self, display):
        display.menu()
        choix = int(
            input("Taper 1 pour continuer, " +
                  "2 list players, 3 classement des joueurs=> "))
        while choix != 1:
            if choix == 2:
                all_p = table_players.all()
                display.display_players(all_p)
            elif choix == 3:
                rang = table_tournois.get(doc_id=1)["players"]
                display.display_ranking(rang)
            choix = int(
                input("Taper 1 pour continuer, " +
                      "2 list players, 3 classement des joueurs= > "))

    def create_tournament(self):
        name_tournois = input("Entrer le nom du tournois => ")
        name_ville = input("Enter de la ville => ")
        time_control = input(
            "Enter time control type [speed, bullet or blitz] => ")

        while time_control not in ["speed", "blitz", "bullet"]:
            time_control = input(
                "Please enter one of the list: speed, bullet or blitz: ")
        description_tournois = input("Enter la description du tournois => ")

        tournament = Tournament(name_tournois, name_ville,
                                time_control, description_tournois)
        table_tournois.insert(tournament.serialized_tournaments)

        return tournament

    def main_game(self, display):
        display.welcome()
        main_message = int(
            input("Pour creer un Tournoi taper 1" +
                  "ou 2 pour Quitter le jeu= > "))
        while (main_message not in [1, 2]):
            main_message = int(input("Choisir le bon chiffre 1 ou 2 => "))
        if main_message == 1:
            return self.create_tournament()
        else:
            exit()

    def update_listmatch(self, match, element):
        Qmatch = Query()
        table_rounds.update(add(
            'list_match', match.serialized_match["vs_Players"]),
            Qmatch.name == element.name)

    def update_classement(self, tournois):
        Q_tournois = Query()
        rank_players = {k.last_name: v for k,
                        v in
                        tournois.serialized_tournaments["players"].items()}
        table_tournois.update({'players': rank_players},
                              Q_tournois.name == tournois.name)

    def add_table_match(self, round):
        for match in round.list_match:
            table_match.insert(match.serialized_match)

    def create_players(self, tournois):  # Pour cree les joueurs
        p = []
        for _ in range(9):
            last_name, first_name, birth_date, gender = input(
                "Entrer last name, first name, birth date, gender : ").split()
            player = Players(last_name, first_name, birth_date, gender)
            p.append(player.serialized_players)
            tournois.players[player] = 0
        table_players.insert_multiple(p)

    def create_round(self, tournois):  # Creation de round
        for i in range(tournois.number_of_turns):
            name = "Round_{}".format(i)
            rounds = Round(name)
            tournois.list_of_rounds.append(rounds)
            table_rounds.insert(rounds.serialized_rounds)
        return rounds

# Calcul le point quand tu gagne un match , perd un match, ou égalité

    def player_points(self, tournois, element):

        for m in element.list_match:
            if list(m.vs_players.values())[0] > list(m.vs_players.values())[1]:
                tournois.players[m.player1] += 1

            elif list(m.vs_players.values())[0] == \
                    list(m.vs_players.values())[1]:
                tournois.players[m.player1] += 0.5
                tournois.players[m.player2] += 0.5
            else:
                tournois.players[m.player2] += 1

    def score_players(self, round, display):
        for numb, element in enumerate(round.list_match):
            element.vs_players[list(element.vs_players.keys())[0]] = int(input(
                "Match{} score joueur1: ".format(numb)))
            element.vs_players[list(element.vs_players.keys())[1]] = int(input(
                "Match{} score joueur2: ".format(numb)))
        display.round_end(round.name)

    def match_exist(self, tournois, p1, p2):
        for e in tournois.list_of_rounds:
            for element in e.list_match:
                if p1 in list(element.vs_players.keys()) \
                        and p2 in list(element.vs_players.keys()):
                    return True
        return False

# L'ensemble de triage score et les match déjà existant
    def match_creation_system(self, tournois, index, element):
        list_players = sorted(tournois.players.items(), key=lambda t: t[1])
        list_players.reverse()

        for _ in range(len(list_players)//2):
            for i in range(1, len(list_players)//2):
                if not self.match_exist(tournois, list_players[0][0],
                                        list_players[i][0]):
                    name = input("Entrer le nom du match => ")
                    match = Match(name, list_players[0][0], list_players[i][0])
                    tournois.list_of_rounds[index].list_match.append(match)
                    self.update_listmatch(match, element)

                    if len(list_players) > 2:
                        list_players = list(set(list_players) -
                                            {list_players[0], list_players[i]})

        name = input("Entrer le nom du match => ")
        match = Match(name, list_players[0][0], list_players[1][0])
        tournois.list_of_rounds[index].list_match.append(match)
        self.update_listmatch(match, element)
        print("\n")

# Pour voir si un joueur a déjà joué dans un match

    def run_system_suisse(self, tournois, display):
        half_player = list(tournois.players.keys())[
            len(tournois.players.keys()) // 2:]
        all_p = table_players.all()
        display.display_players(all_p)
        for index, element in enumerate(tournois.list_of_rounds):
            if index == 0:
                for i in range(len(tournois.players.keys()) // 2):
                    name = input("Entrer nom du match => ")
                    match = Match(name, list(tournois.players.keys())
                                  [i], half_player[i])

                    tournois.list_of_rounds[index].list_match.append(match)
                    self.update_listmatch(match, element)

                print("\n")
                Q_m = Query()
                m = table_rounds.search(Q_m.name == element.name)
                display.display_vs(m)
                self.menu_match(display)
                self.score_players(element, display)
                self.player_points(tournois, element)
                self.add_table_match(element)
                self.update_classement(tournois)

            else:
                self.match_creation_system(tournois, index, element)
                Q_m = Query()
                m = table_rounds.search(Q_m.name == element.name)
                display.display_vs(m)
                self.menu_match(display)
                self.score_players(element, display)
                self.player_points(tournois, element)
                self.add_table_match(element)
                self.update_classement(tournois)
