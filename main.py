from controllers import Controller
from views import AView

display = AView()
control = Controller()

if __name__ == "__main__":
    tournois_paris = control.main_game(display)
    control.create_players(tournois_paris)
    control.create_round(tournois_paris)
    control.run_system_suisse(tournois_paris, display)
